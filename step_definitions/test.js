const {
  client
} = require('nightwatch-cucumber');
const {
  Given,
  Then,
  When
} = require('cucumber');
Given(/^User opens URL "(.*?)"$/, (url) => {
  return client.url(url).waitForElementVisible('body', 7000);
});
When(/^User enters Postcode as "(.*?)"$/, (ps) => {
  return client.setValue('input[id="address"]', ps).saveScreenshot('test_ss/postcode.png').waitForElementVisible('#suggestBoxElement', 3000).assert.visible('#suggestBoxElement');
});
When(/^Select the "(.*?)" option from the dropdown$/, (sub) => {
  const postcodeDropdownSelector = '#pcId4';
  return client.saveScreenshot('test_ss/dropdown.png').pause(1000).click(postcodeDropdownSelector).saveScreenshot('test_ss/dropdown2.png'); //[@id="start-select-form"]/button 
});

When(/^Click on start$/, () => { return client.pause(2000).useXpath().click('//*[@id="start-select-form"]/button'); }); 
Then(/^Page title should be "(.*?)"$/, (title2) => { return client.useCss().saveScreenshot('test_ss/page2.png').assert.title(title2); }); 

When('User selects {string},{string},{string},{string}', function (productType, propertyType, propertyOwnership, moveIn) 
{ const productTypeSelector = 'input[name="vertRBtn"][value =' + productType + ']'; 
const propertyTypeSelector = 'input[name="customerType"][value =' + propertyType + ']'; 
const propertyOwnershipSelector = 'input[name="propertyOwner"][value =' + propertyOwnership + ']'; 
const moveInStatusSelector = 'input[name="preference"][value =' + moveIn + ']'; 
return client.click(productTypeSelector).click(propertyTypeSelector).click(propertyOwnershipSelector).click(moveInStatusSelector).saveScreenshot('test_ss/pagesamp.png'); }); 
When('User selects electricty bill as {string} and gas bill as {string}', function (electricityBill, gasBill) { const electricityBillSelector = 'input[name="elecScenario"][value ="' + electricityBill + '"]'; const gasBillSelector = 'input[name="gasScenario"][value ="' + gasBill + '"]'; return client.click(electricityBillSelector).click(gasBillSelector).saveScreenshot('test_ss/pagesamp1.png').pause(2000); }); 
When('User selects solarpanels as {string}, electricity provider as {string}, electricity usage as {string}', function (solarPanel, elecProvider, elecUsage) 
{ const solarPanelSelector = 'input[name="solarpanel"][value ="Yes"]'; 
  const solarPanelSelector1 = '.mrgnT20 > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > input:nth-child(2)'; 
  const elecProviderSelector = '#elecproviderlist_usage option[value=' + elecProvider + ']'; 
  const elecUsageSelector = 'input[type="radio"][name="UsageLevel"][value = ' + elecUsage + ']'; 
  return client.waitForElementVisible('#elecproviderlist_usage', 4000)
  .pause(4000)
  .useXpath().click('/html/body/div[2]/div[2]/div[2]/div/div[1]/form/div[4]/div/fieldset/div[1]/div[2]/div/div[1]/input')
  .useCss().click(elecProviderSelector).click(elecUsageSelector).saveScreenshot('test_ss/pagesamp2.png').pause(3000); }); 
When('User selects gas provider as {string}, gas usage as {string}', function (gasProvider, gasUsage) { const gasProviderSelector = '#gasproviderlist_usage option[value=' + gasProvider + ']'; const gasUsageSelector = 'input[type="radio"][name="gasUsageLevel"][value = ' + gasUsage + ']'; return client.click(gasProviderSelector).click(gasUsageSelector).saveScreenshot('test_ss/pagesamp3.png').pause(3000); }); 
When('User checks the two agreements', function () { return client.click('#valAgree').click('#privacyAgree').saveScreenshot('test_ss/pagesamp4.png'); }); 
When('User clicks on continue', function () { return client.click('#formButton').saveScreenshot('test_ss/pagesamp5.png'); });